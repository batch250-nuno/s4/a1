package com.zuitt.example;
// Implement Action Interface
public class Person implements Actions, Greetings {
//    Actions
    @Override
    public void sleep() {
        System.out.println("Zzz...");
    }
    @Override
    public void run() {
        System.out.println("Running...");
    }
// Greetings
    @Override
    public void morningGreet() {
        System.out.println("Ohayo!");
    }

    @Override
    public void holidayGreet() {
        System.out.println("Happy Holiday!");
    }
}
