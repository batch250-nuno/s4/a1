package com.zuitt.example;
// Child Class of Animal
    //"extends"
public class Dog extends Animal {
    // Properties / Variables
    private String breed;

    // Constructor
    public Dog() {
        // have direct access to origin constructor
        super();
        this.breed = "Husky";
    }

    public Dog(String name, String color, String breed) {
        super(name, color);
        this.breed = breed;
    }

    // Setter and Getter
    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }

    // Method
    public void bark() {
        System.out.println("Woof");
    }
    public void call() {
//        super.call();
        System.out.println("Hi my name is " + this.name + ", I am a dog.");
    }
}
