package com.zuitt.example;

public class Car {
    //Access Modifier
        // Default
        // Private
        // Protected
        // Public

    //Class Creation
    // 1. Properties / Variable
     private String name;
     private String brand;
     private int yearMade;

     // Composition Approach
     private Driver driver;


     // 2. Constructor - instantiate object
        // empty constructor
        public Car() {
            this.yearMade = 2000;
            // Default for New Drive
            this.driver = new Driver("Alexa");
        };

        // parameterized constructor
        public Car(String name, String brand, int yearMade) {
            this.name = name;
            this.brand = brand;
            this.yearMade = yearMade;
            this.driver = new Driver();
        };

    // 3. Getter and Setter
        // Getters
        public String getName() {
            return this.name;
        }
        public String getBrand() {
            return this.brand;
        }
        public int getYearMade() {
            return this.yearMade;
        }
        public String getDriverName() {
            return this.driver.getName();
        }

        // Setters
        public void setName(String name) {
            this.name = name;
        }
        public void setBrand(String brand) {
            this.brand = brand;
        }
        public void setYearMade(int yearMade) {
            // can add validation
            if (yearMade <= 2022) {
                this.yearMade = yearMade;
            }
        }
        public void setDriver(String driver) {
            // Invoke setName() of driver class
            this.driver.setName(driver);
        }

    // 4. Methods (optional)
    public void drive() {
        System.out.println("This car is running...");
    }


}
