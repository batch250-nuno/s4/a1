package com.zuitt.example;

public class StaticPoly {
    // Original Form
    public int addition(int a, int b) {
      return a + b;
    };

    // Overloading By Changing the Number of Args
    public int addition(int a, int b, int c) {
        return a + b + c;
    };

    // Overloading By Changing the Data Type
    public double addition(double a, double b) {
        return a + b;
    };

}
