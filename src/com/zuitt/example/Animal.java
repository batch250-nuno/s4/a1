package com.zuitt.example;
// Parent Class
public class Animal {
    // Properties / Variable
    protected String name;
    private String color;

    // Constructor
    public Animal(){};
    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    };

    // Getter and Setter
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    // Method
    public void call() {
        System.out.println("Hi my name is " + this.name);
    }
}
