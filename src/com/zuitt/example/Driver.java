package com.zuitt.example;

public class Driver {
    // Properties / Variables
    private String name;

    // Constructors
    public Driver() {}
    public Driver(String name) {
        this.name = name;
    }

    // Getter and Setter
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
